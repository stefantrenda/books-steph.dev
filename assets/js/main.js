let books = [
    {
        title: 'The Hobbit',
        autor: 'J.R.R. Tolkien',
        maxPages: '200',
        onPage: '60',
    },
    {
        title: 'Harry Potter',
        autor: 'J.K. Rowling',
        maxPages: '250',
        onPage: '150',
    },
    {
        title: '50 Shades of Grey',
        autor: 'E.L. James',
        maxPages: '150',
        onPage: '150',
    },
    {
        title: 'Don Quiote',
        autor: 'Miguel de Cervantes',
        maxPages: '350',
        onPage: '300',
    },
    {
        title: 'Hamlet',
        autor: 'William Shakespeare',
        maxPages: '550',
        onPage: '550',
    },
];

const allBooks = document.querySelector('#info-book');
const checkIfRead = document.querySelector('#is-read')

const bookForm = document.querySelector('form');
const addBookBtn = document.querySelector('.add-book');
const inputs = document.querySelectorAll('input');
const inputTitle = document.querySelector('.title');
const inputAuthor = document.querySelector('.author');
const inputMaxPage = document.querySelector('.maxpage');
const inputCurrentPage = document.querySelector('.onpage')

const tBook = document.querySelector('.table');
const tBody = document.querySelector('.table-body')

function Precent(partial, total) {
    return Math.floor((100 * partial) / total);
}


const everyBooks = books.forEach((book) => {
    let infoUl = document.createElement('ul');
    let infoLi = document.createElement('li');
    let checkUl = document.createElement('ul');
    let checkLi = document.createElement('li');

    let bookPrecent = Precent(book.onPage, book.maxPages);
    
    infoLi.textContent += `${book.title} by ${book.autor}`
    
    infoUl.appendChild(infoLi);
    allBooks.appendChild(infoUl);


    if(book.maxPages === book.onPage) {
        checkLi.textContent += `You already read ${book.title} by ${book.autor}`
        checkLi.classList.add('text-success');
    } else {
        checkLi.textContent += `You still need to read ${book.title} by ${book.autor}.`
        checkLi.classList.add('text-danger');
    }

    checkUl.appendChild(checkLi);
    checkIfRead.appendChild(checkUl);

    function addTableData(data) {
        const myBar = document.createElement('div');
        const myProgress = document.createElement('div');

        let tRow = tBook.insertRow();
        let title = tRow.insertCell(0);
        let autor = tRow.insertCell(1);
        let maxPages = tRow.insertCell(2);
        let currentPage = tRow.insertCell(3);
        let progressBar = tRow.insertCell(4);

        title.innerHTML = book.title;
        autor.innerHTML = book.autor;
        maxPages.innerHTML = book.maxPages;
        currentPage.innerHTML = book.onPage;
        
        myBar.setAttribute('id', 'myBar');
        myProgress.setAttribute('id', 'myProgress');
        myBar.textContent = `${bookPrecent}%`;
        myBar.style.width = `${bookPrecent}%`;
        myProgress.append(myBar);
        progressBar.append(myProgress);

    }

    addTableData(books);
});


function newBook(e) {
    e.preventDefault();

    const authorValue = inputAuthor.value;
    const titleValue = inputTitle.value;
    const currentPageValue = inputCurrentPage.value;
    const maxPageValue = inputMaxPage.value;

    if (currentPageValue > maxPageValue) {
        alert(`Please enter a correct page namber.`);
        return
    };

    if (!authorValue || !titleValue || !currentPageValue || !maxPageValue) {
        alert(`Please fill in all inputs`)
        return
    };

    let tr = document.createElement('tr');
    let titleTd = document.createElement('td');
    let authorTd = document.createElement('td');
    let maxPagesTd = document.createElement('td');
    let onPageTd = document.createElement('td');
    let progressBarTd = document.createElement('td');
    let getPrecent = Precent(inputCurrentPage.value, inputMaxPage.value);
    let myBar = document.createElement('div');
    let myProgress = document.createElement('div');

    titleTd.textContent = inputTitle.value;
    authorTd.textContent = inputAuthor.value;
    maxPagesTd.textContent = inputMaxPage.value;
    onPageTd.textContent = inputCurrentPage.value;
    myBar.setAttribute('id', 'myBar');
    myProgress.setAttribute('id', 'myProgress');
    myBar.textContent = `${getPrecent}%`;
    myBar.style.width = `${getPrecent}%`;
    myProgress.append(myBar);
    progressBarTd.append(myProgress);
    tr.append(titleTd, authorTd, maxPagesTd, onPageTd, progressBarTd);
    tBody.append(tr);

    inputs.forEach((e) => {
        e.value = "";
    });
   
}
addBookBtn.addEventListener('click', newBook);

